package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;

public class ClubTest {

	public void testEnter() {
		Person p1 = new Person("Ilyas", "Salman",
				LocalDate.parse("1990-12-12"), Person.Sex.MALE);

		Person p2 = new Person("Dominik", "Hyper",
				LocalDate.parse("1994-10-07"), Person.Sex.MALE);

		Person p3 = new Person("Heinz", "Dexter",
				LocalDate.parse("1996-12-01"), Person.Sex.MALE);

		Club c = new Club();
		boolean erwartet = true;
		boolean erg = c.enter(p1);
		c.enter(p2);
		c.enter(p3);
		assertEquals(erwartet, erg);

		System.out.println("test");

		
	}

	public void testNumberOf() {
		Club c = new Club();
		int erg = c.numberOf();
		int erwartet = 0;
		assertEquals("0", erg, erwartet);

	}

	public void testgetSchnitt() {

		Club c = new Club();
		c.getSchnitt();
		double erwartet = 42;
		double erg = c.getSchnitt();
		assertEquals(erwartet, erg);
	}

	public void testSortierenAlter() {
		Club c = new Club();

		c.sortierenAlter();

		boolean b = c
				.getPersons()
				.get(0)
				.getBirthday()
				.isBefore(
						(c.getPersons().get(c.getPersons().size() - 1)
								.getBirthday()));
		System.out.println(c.drucken());

		assertEquals(true, b);
	}

	public void testSortierenName() {
		Club c = new Club();

		c.sortierenName();

		int x = c
				.getPersons()
				.get(0)
				.getLastName()
				.compareTo(
						c.getPersons().get(c.getPersons().size() - 1)
								.getLastName());
		System.out.println(c.drucken());
		boolean b = 0 > x;

		System.out.println(x);

		assertEquals(true, b);
	}

	public void testLeave(int index) {
		Person p2 = new Person("Dominik", "Hyper",
				LocalDate.parse("1994-10-07"), Person.Sex.MALE);
		Club c = new Club();
		c.enter(p2);
		System.out.println(c.drucken());
		c.tschau(p2);
		boolean erwartet = true;
		boolean erg = c.tschau(p2);
		assertEquals(erwartet, erg);
	}

}
