package at.spenger.junit.domain;

import java.util.Comparator;

public class SortName implements Comparator<Person> {


	 @Override
	public int compare(Person p1, Person p2) {
		
			String s1 = p1.getFirstName() + " " + p1.getLastName();
			String s2 = p2.getFirstName() + " " + p2.getLastName();
			return s1.compareTo(s2);
	}
}