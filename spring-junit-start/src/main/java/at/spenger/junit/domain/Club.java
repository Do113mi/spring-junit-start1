package at.spenger.junit.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Club {
	private List<Person> l;

	public Club() {
		l = new ArrayList<>();
	}

	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}

	public boolean tschau(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		if (l.contains(p))
			return l.remove(p);
		return false;
	}

	public int numberOf() {
		return l.size();
	}

	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}

	public float getSchnitt() {
		float alt = 0;
		for (Person p : l) {
			alt += LocalDateTime.now().getYear() - p.getBirthday().getYear();
		}
		alt = alt / l.size();
		return alt;
	}

	public void sortierenName() {
		Collections.sort(l, new SortName());
	}

	public void sortierenAlter() {
		Collections.sort(l, new SortAlter());
	}

	public String drucken() {
		StringBuilder b = new StringBuilder();
		b.append(System.lineSeparator());
		b.append("Es gibt: " + l.size() + " Leute");
		b.append(System.lineSeparator());
		
		for (Person p : l) {
			b.append("Name: " + p.getFirstName() + " " + p.getLastName()
					+ ", Alter: " + p.getBirthdayString());
		}

		return b.toString();
		
	}

}
